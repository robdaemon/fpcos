# OS

A FreePascal based kernel.

## Requirements

* [FreePascal 3.0.0](http://www.freepascal.org/download.var)
* [nasm](http://www.nasm.us/)
* [qemu](http://wiki.qemu.org/Main_Page)
* [GNU Make](https://www.gnu.org/software/make/)

## Building

```bash
cd src
make all
```

## Running

```bash
cd src
make qemu
```

## License

Licensed under the MIT License. See [LICENSE](LICENSE)
