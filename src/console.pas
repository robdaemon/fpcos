unit console;

interface

var
   xpos: Integer = 0;
   ypos: Integer = 0;

type
   ConsoleColors = (Black, Blue, Green, Cyan, Red, Magenta, Brown, LightGray,
                   DarkGray, LightBlue, LightGreen, LightCyan, LightRed, LightMagenta, LightBrown, White);

procedure kclearscreen();
procedure kwritechr(c: Char; color: Byte);
procedure kwritestr(s: PChar; color: Byte);
procedure kwriteint(i: Integer; color: Byte);
procedure kwritedword(i: DWORD; color: Byte);
function makecolor(fg: ConsoleColors; bg: ConsoleColors): Byte;

implementation

var
   vidmem: PChar = PChar($b8000);

function makecolor(fg: ConsoleColors; bg: ConsoleColors): Byte;
begin
   makecolor := ord(fg) xor ord(bg) shl 8;
end;

procedure kclearscreen(); [public, alias: 'kclearscreen'];
var
   i: Integer;
begin
   for i := 0 to 3999 do
   begin
      vidmem[i] := #0;
   end;
end;

procedure kwritechr(c: Char; color: Byte); [public, alias: 'kwritechr'];
var
   offset: Integer;
begin
   if (ypos > 24) then
   begin
      ypos := 0;
   end;

   if (xpos > 79) then
   begin
      xpos := 0;
   end;

   offset := (xpos shl 1) + (ypos * 160);
   vidmem[offset] := c;
   offset += 1;
   vidmem[offset] := #7;
   offset += 1;

   xpos := (offset mod 160);
   ypos := (offset - ypos) div 160;
   xpos := xpos shr 1;
end;

procedure kwritestr(s: PChar; color: Byte); [public, alias: 'kwritestr'];
var
   offset, i: Integer;
begin
   if (ypos > 24) then
   begin
      ypos := 0;
   end;

   if (xpos > 79) then
   begin
      xpos := 0;
   end;

   offset := (xpos shl 1) + (ypos * 160);
   i := 0;

   while (s[i] <> Char($0)) do
   begin
      vidmem[offset] := s[i];
      offset += 1;
      vidmem[offset] := Char(color);
      offset += 1;
      i += 1;
   end;

   xpos := (offset mod 160);
   ypos := (offset - ypos) div 160;
   xpos := xpos shr 1;
end;

procedure kwriteint(i: Integer; color: Byte); [public, alias: 'kwriteint'];
var
   buffer: array[0..11] of Char;
   str: PChar;
   digit: DWORD;
   minus: Boolean;
begin
   str := @buffer[11];
   str^ := #0;

   if (i < 0) then
   begin
      digit := -i;
      minus := True;
   end else begin
      digit := i;
      minus := False;
   end;

   repeat
      Dec(str);
      str^ := Char((digit mod 10) + Byte('0'));
      digit := digit div 10;
   until (digit = 0);

   if (minus) then
   begin
      Dec(str);
      str^ := '-';
   end;

   kwritestr(str, color);
end;

procedure kwritedword(i: DWORD; color: Byte); [public, alias: 'kwritedword'];
var
   buffer: array[0..11] of Char;
   str: PChar;
   digit: DWORD;
begin
   for digit := 0 to 10 do
   begin
      buffer[digit] := '0';
   end;

   str := @buffer[11];
   str^ := #0;

   digit := i;
   repeat
      Dec(str);
      str^ := Char((digit mod 10) + Byte('0'));
      digit := digit div 10;
   until (digit = 0);

   kwritestr(@buffer[0], color);
end;

end.
