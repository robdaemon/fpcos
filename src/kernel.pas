unit kernel;

interface

uses
   multiboot,
   console;

procedure kmain(mbinfo: Pmultiboot_info_t; mbmagic: DWORD); stdcall;

implementation

procedure kmain(mbinfo: Pmultiboot_info_t; mbmagic: DWORD); stdcall; [public, alias: 'kmain'];
begin
   kclearscreen();
   kwritestr('barebones', makecolor(LightGreen, Black));
   xpos := 0;
   ypos += 1;

   if (mbmagic <> MULTIBOOT_BOOTLOADER_MAGIC) then
   begin
      kwritestr('Halting, a multiboot-compliant boot loader is required', makecolor(White, Red));
      asm
         cli
         hlt
      end;
   end else begin
      kwritestr('Booted', makecolor(LightBlue, Black));
      xpos := 0;
      ypos += 2;
      kwritestr('Multiboot information', makecolor(LightGray, Black));
      xpos := 0;
      ypos += 2;
      kwritestr('                       Lower memory  = ', makecolor(LightGray, Black));
      kwriteint(mbinfo^.mem_lower, makecolor(LightGray, Black));
      kwritestr('KB', makecolor(LightGray, Black));
      xpos := 0;
      ypos += 1;
      kwritestr('                       Higher memory = ', makecolor(LightGray, Black));
      kwriteint(mbinfo^.mem_upper, makecolor(LightGray, Black));
      kwritestr('KB', makecolor(LightGray, Black));
      xpos := 0;
      ypos += 1;
      kwritestr('                       Total memory  = ', makecolor(LightGray, Black));
      kwriteint(((mbinfo^.mem_upper + 1000) div 1024) +1, makecolor(LightGray, Black));
      kwritestr('MB', makecolor(LightGray, Black));
   end;

   asm
      @loop:
         jmp @loop
   end;
end;

end.
